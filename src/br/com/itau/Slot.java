package br.com.itau;

import java.util.Objects;
import java.util.Random;

public class Slot {

    private Opcoes opcao;

    public Slot() {
        Random aleatorio = new Random();
        int pontuacao = Opcoes.values().length;
        int pontuacaoRandom = aleatorio.nextInt(pontuacao);

        this.opcao = Opcoes.values()[pontuacaoRandom];

    }

    @Override
    public String toString() {
        String modelo = "Slot:" +
                " " + opcao +
                " pontos: " + opcao.pontos;
        return modelo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slot slot = (Slot) o;
        return opcao == slot.opcao;
    }

    @Override
    public int hashCode() {
        return Objects.hash(opcao);
    }


    public Opcoes getOpcao() {
        return opcao;
    }

    public void setOpcao(Opcoes opcao) {
        this.opcao = opcao;
    }
}
