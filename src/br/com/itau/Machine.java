package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Machine {

    private List<Slot> slots;


    public Machine(int nivel){
        this.slots = new ArrayList<>();

        for (int i = 0; i < nivel; i++){
            slots.add(new Slot());
        }

    }

    public int calcularPontuacao(){
        int pontuacao = 0;
        for (Slot slot:slots) {
            pontuacao = pontuacao + slot.getOpcao().pontos;
        }
        if(exitEBonus()){
            pontuacao = pontuacao*100;
        }

        return pontuacao;
    }

    public boolean exitEBonus(){
        boolean resposta = slots.stream().distinct().limit(slots.size()).count() == 1;
        return resposta;
    }

    @Override
    public String toString() {
        return "Maquina" + slots +
                '}';
    }



    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }
}
